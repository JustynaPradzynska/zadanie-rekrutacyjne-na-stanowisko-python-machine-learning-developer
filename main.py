from queue import Queue
from threading import Thread
import time
import os
import cv2
import numpy as np


class Source:
    def __init__(self, source_shape: tuple):
        self._source_shape: tuple = source_shape

    def get_data(self) -> np.ndarray:
        rows, cols, channels = self._source_shape
        return np.random.randint(
            256,
            size=rows * cols * channels,
            dtype=np.uint8,
        ).reshape(self._source_shape)


def producer(out_a):
    """Every 50ms Producer gets new data"""
    i = 0
    while i < 100:
        data = Source((1024, 768, 3)).get_data()
        time.sleep(0.05)
        # Puts data in queue A
        out_a.put(data)
        i += 1
    out_a.task_done()


def consumer(in_a, out_b):
    """The Consumer gets the data from queue A and does the processing"""
    j = 0
    while j < 100:  # True:
        # Consumer gets some data
        data = in_a.get()
        # Consumer processes the data
        data_resize = data[::2, ::2]  # Double image reduction
        kernel = np.ones((5, 5), np.float32) / 25  # Median filter with a 5x5 kernel
        dst = cv2.filter2D(data_resize, -1, kernel)  # Apply filter
        out_b.put(dst)
        j += 1
    out_b.task_done()


if __name__ == "__main__":
    DIRECTORY = 'processed'
    path = os.path.join(os.getcwd(), DIRECTORY)
    try:
        os.mkdir(path)
    except OSError:
        print(f"Creation of the directory {path} failed.")
    else:
        print(f"Successfully created the directory {path}.")

    # Create the A and B queue and launch both threads
    A = Queue()
    B = Queue()
    t1 = Thread(target=consumer, args=(A, B))
    t2 = Thread(target=producer, args=(A,))
    t1.start()
    t2.start()

    # Wait for all data to be processed
    A.join()
    B.join()
    t2.join()
    t1.join()

    for file in range(100):
        # Creating png files within 'processed' folder
        FILENAME = str(file) + '.png'
        cv2.imwrite(os.path.join(path, FILENAME), B.get())
